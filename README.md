# Found demo version here http://sweetdream-helmo.weebly.com/my-work.html #

# README #

### TO-DO ###

* Improve tests

### For fun ###

* ParametersScene
* Languages options
* Level with bonus at level 10 and 30

### For professor ###

Add Slick2D/lib/jogg-0.0.7.jar and Slick2D/lib/jorbis-0.0.7.jar to buildpath

### Etat d'avancement ###

* F0 - verify
* F1 - verify
* F2 - verify
* F3 - verify
* F4 - verify
* F5 - verify

### Questions ###

* Medicine collection -> Pourquoi un Set
--> ordre pas important et pas de doublon

* GameParameters -> Pourquoi pas final
--> Reprendre une partie doit pouvoir changer la taille de la grille, et du coup influencé les autres valeurs (setGridSize(int v))

* BD saveGame -> Pourquoi pas de jointure entre mscol_grid et mscol_game
--> Jointure pens�e pour les medicines, mais je stocke ceux-ci dans un string tout comme la grille

* Si tu pense à quelque chose note le ici avec/sans réponse