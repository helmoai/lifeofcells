package org.helmo.cellsoflife.domains;

import static org.junit.Assert.*;

import org.helmo.cellsoflife.utils.ArrayXDimension;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class ArrayXDimensionTest {
	
	private static int[][] array = {
			{1, 2, 3, 4, 5},
			{6, 7, 8, 9, 10},
			{11, 12, 13, 14, 15},
			{16, 17, 18, 19, 20},
			{21, 22, 23, 24, 25}
	};
	
	// Pos
	//	5 6 7
	//	3 X 4
	//	0 1 2
	
	@Parameters(name="IndexOutOfBound Exception")
	public static Object[][] getDatas()	{
		return new Object[][]	{
			{new int[]{10, 6, 7, 5, 2, 25, 21, 22}, array, 0, 0},
			{new int[]{20, 16, 17, 15, 12, 10, 6, 7}, array, 2, 0},
			{new int[]{7, 8, 9, 2, 4, 22, 23, 24}, array, 0, 2},
			{new int[]{17, 18, 19, 12, 14, 7, 8, 9}, array, 2, 2},
			{new int[]{23, 24, 25, 18, 20, 13, 14, 15}, array, 3, 3}
		};
	}
	
	private int[] expectedInt;
	private int[][] arrayInt;
	private int i;
	private int j;
	
	public ArrayXDimensionTest(int[] exp, int[][] pArray, int row, int col) {
		expectedInt = exp;
		arrayInt = pArray;
		i = row;
		j = col;
	}
	
	@Test
	public void testGetAroundPosFromSquareArrayInt() {
		assertArrayEquals(expectedInt, ArrayXDimension.getAroundPosFromSquareArray(i, j, arrayInt));
	}

}
