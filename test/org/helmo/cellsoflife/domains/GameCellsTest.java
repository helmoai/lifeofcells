package org.helmo.cellsoflife.domains;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.helmo.cellsoflife.domains.entity.BattleShip;
import org.helmo.cellsoflife.domains.entity.Medicine;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.junit.Test;

public class GameCellsTest {

	@Test
	public void medicineMoveUpTest() {
		
		GameParameters.loadStructures();
		GameCells gc = new GameCells();
		BattleShip ship = gc.getShip();
		Set<Medicine> medic = new HashSet<>();
		int y = GameParameters.RIGHT_LIMIT + GameParameters.CELLS_SPACE;
		
		medic.addAll(ship.shootMedicine(5));
		Iterator<Medicine> it = medic.iterator();
		
		if (it.hasNext())	{
			
			Medicine me = it.next();
			
			assertEquals(ship.getX(), me.getX());
			assertEquals(y, me.getY());
			
			me.moveUp();
			
			assertEquals(y-GameParameters.CELLS_SPACE, me.getY());
		} else {
			fail();
		}
	}

}
