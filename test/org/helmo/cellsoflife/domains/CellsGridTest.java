package org.helmo.cellsoflife.domains;

import static org.junit.Assert.*;

import org.helmo.cellsoflife.domains.entity.CellsGrid;
import org.junit.Test;

public class CellsGridTest {
	
	private boolean[][] expected1 = {
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, false, false, false, false}
	};
	private boolean[][] expected2 = {
			{false, false, false, false, false},
			{false, false, false, false, false},
			{false, true, true, true, false},
			{false, false, false, false, false},
			{false, false, false, false, false}
	};
	private boolean[][] expected3 = {
			{false, false, false, false, false},
			{true, false, false, false, true},
			{true, false, false, false, true},
			{true, false, false, false, true},
			{false, false, false, false, false}
	};
	private boolean[][] start1 = {
			{false, false, false, false, false},
			{true, false, false, false, false},
			{false, false, false, false, false},
			{false, false, true, false, false},
			{false, false, false, false, false}
	};
	private boolean[][] start2 = {
			{false, false, false, false, false},
			{false, false, true, false, false},
			{false, false, true, false, false},
			{false, false, true, false, false},
			{false, false, false, false, false}
	};
	private boolean[][] start3 = {
			{false, false, false, false, false},
			{false, false, false, false, false},
			{true, true, false, true, true},
			{false, false, false, false, false},
			{false, false, false, false, false}
	};

	@Test
	public void test() {
		
		CellsGrid c1 = new CellsGrid(start1);
		CellsGrid c2 = new CellsGrid(start2);
		CellsGrid c3 = new CellsGrid(start3);
		
		c1.updateCells();
		c2.updateCells();
		c3.updateCells();
		
		assertArrayEquals(expected1, c1.getGrid());
		assertArrayEquals(expected2, c2.getGrid());
		assertArrayEquals(expected3, c3.getGrid());
	}

}
