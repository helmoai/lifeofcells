package org.helmo.cellsoflife.domains;

import static org.junit.Assert.*;

import org.helmo.cellsoflife.domains.entity.CellsGrid;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.helmo.cellsoflife.exception.InvalideArrayException;
import org.junit.Test;

public class CellsGridFactoryTest {
	
	@Test
	public void noParameterForGrid() {
		
		GameParameters.loadStructures();
		
		
			
			CellsGrid grid = new CellsGrid();
			double expected = grid.getContamination();
			assertTrue(expected >= 10);
			
		
	}
	
	@Test
	public void DuplicateGrid() {
		
		boolean[][] expected = {
				{true , true , true , false, false},
				{true , false, false, false, false},
				{true , false, false, false, false},
				{false, false, false, true, true},
				{false, false, false, true, false}
		};
		
		
			CellsGrid grid = new CellsGrid(expected);
			
			assertArrayEquals(expected, grid.getGrid());
		
	}
	
	@Test
	public void DuplicateGridNull() {
		
		boolean[][] expected = null;
		
		try {
			CellsGrid grid = new CellsGrid(expected);
			
			assertArrayEquals(expected, grid.getGrid());
		} catch (InvalideArrayException ex){
			assertNotNull(ex);
		}
	}
	
	@Test
	public void DuplicateGridEmpty() {
		
		boolean[][] expected = {};
		
		try {
			CellsGrid grid = new CellsGrid(expected);
			assertArrayEquals(expected, grid.getGrid());
			
		} catch (InvalideArrayException ex){
			assertNotNull(ex);
		}
	}

}
