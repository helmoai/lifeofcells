package org.helmo.cellsoflife.scenes;

import java.awt.Font;

import org.helmo.cellsoflife.db.CellsDataBase;
import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;


public class WelcomeScene implements Scene {
	
	private String info = "";

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image back = new Image("ressources/w.png");
		Font font = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 20);
		Font bigFont = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 40);
		TrueTypeFont ttf = new TrueTypeFont(font, true);
		
		back.draw(0, 0, 640, 480);
		
		painter.setFont(ttf);
		painter.setColor(Color.red);
		painter.drawString(info, 100, 390);
		painter.setColor(Color.black);
		painter.drawString(GameLanguage.currentLanguage.get("parameters"), 100, 420);
		painter.drawString(GameLanguage.currentLanguage.get("resume"), 360, 420);
		painter.drawString(GameLanguage.currentLanguage.get("ranking"), 100, 445);
		painter.drawString(GameLanguage.currentLanguage.get("quit"), 360, 445);
		
		ttf = new TrueTypeFont(bigFont, true);
		painter.setFont(ttf);
		painter.drawString(GameLanguage.currentLanguage.get("start"), 170, 205);
	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Input charPressed = gc.getInput();
		Scene s = this;
		
		if (charPressed.isKeyPressed(Keyboard.KEY_Q))	{
			
			gc.exit();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_A))	{
			
			s = new RankingScene();
			s.gainingFocus(gc);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_N))	{
			
			s = new GameScene();
			s.gainingFocus(gc);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_R))	{
			
			String[][] gameSave = CellsDataBase.getSaveGame();
			
			if (gameSave.length > 0)	{
				GameParameters.setGridSize(Integer.parseInt(gameSave[0][4]));
				GameParameters.SPEED_INFECTION = Integer.parseInt(gameSave[0][7]);
				s = new GameScene(gameSave[0][1], gameSave[0][2], gameSave[0][3], gameSave[0][5], gameSave[0][6], gameSave[0][8]);
			} else {
				info = "No game in database, please press N to start a new game";
				//s = new GameScene();
			}
			s.gainingFocus(gc);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_P))	{
			
			s = new ParametersScene();
			
		}
		
		charPressed.clearKeyPressedRecord();
		return s;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
}
