package org.helmo.cellsoflife.scenes;

import org.helmo.cellsoflife.domains.GameCells;
import org.helmo.cellsoflife.domains.entity.BattleShip;
import org.helmo.cellsoflife.domains.entity.Experience;
import org.helmo.cellsoflife.domains.entity.Medicine;
import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.helmo.cellsoflife.utils.Converter;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class GameScene implements Scene {
	
	private GameCells game;
	private GameContainer gameContainer;

	public GameScene(String score, String time, String level, String grid, String medic, String backup) {
		game = new GameCells(Integer.parseInt(score), time, Integer.parseInt(level), Converter.stringtoGrid(grid), Converter.stringToMedic(medic), Converter.stringtoGrid(backup));
	}

	public GameScene() {
		game = new GameCells();
	}
	
	public GameScene(boolean[][] grid)	{
		game = new GameCells(grid);
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image ship = new Image("ressources/ship.png");
		Image laser = new Image("ressources/laser.png");
		Image cell = new Image("ressources/cells.png");
		Image nocell = new Image("ressources/template.png");
		Image back = new Image("ressources/g.png");
		Experience xp = game.getExperience();
		
		back.draw(0, 0, 640, 480);
		
		painter.setColor(new Color(0, 128, 0));
		painter.drawString(GameLanguage.currentLanguage.get("infected") + String.format("%.2f", game.getContamination()) + "%", 450, 20);
		painter.drawString(GameLanguage.currentLanguage.get("score") + game.getScore(), 450, 40);

		if (!gameContainer.isPaused())	{
			painter.drawString(GameLanguage.currentLanguage.get("time") + game.getTime(), 450, 60);
		}
		painter.drawString("-------------------", 450, 90);
		painter.drawString(GameLanguage.currentLanguage.get("move"), 450, 100);
		painter.drawString(GameLanguage.currentLanguage.get("shoot"), 450, 120);
		painter.drawString(GameLanguage.currentLanguage.get("save"), 450, 140);

		painter.drawString(GameLanguage.currentLanguage.get("menu"), 450, 170);
		painter.drawString("Pause: P", 450, 190);
		painter.drawString("-------------------", 450, 210);
		
		painter.setColor(new Color(0, 128, 0));
		painter.fillRect(xp.left(), xp.bottom(), (float) xp.getExperienceAdvancement(), xp.getH());
		
		painter.setColor(Color.black);
		painter.drawRect(xp.left(), xp.bottom(), xp.getLg(), xp.getH());
		painter.drawString(GameLanguage.currentLanguage.get("level") + xp.getLevel(), xp.left()+5, xp.bottom()+8);
		painter.drawString(GameLanguage.currentLanguage.get("bonus") + xp.getBonus(), xp.left(), xp.bottom()+30);
		
		painter.setColor(Color.white);
		for (int i = 0; i < game.getGrid().length; i++)	{
			for (int j = 0; j < game.getGrid()[0].length; j++)	{
				
				int x = (i)*GameParameters.CELLS_SPACE+GameParameters.SPACE_LEFT_WINDOW;
				int y = (j)*GameParameters.CELLS_SPACE+GameParameters.SPACE_LEFT_WINDOW;
				if (game.getGrid()[i][j])	{
					cell.draw(x, y, GameParameters.CELLS_SIZE, GameParameters.CELLS_SIZE);
				} else {
					nocell.draw(x, y, GameParameters.CELLS_SIZE, GameParameters.CELLS_SIZE);
				}
			}
		} 
		
		painter.setColor(Color.green);
		for(Medicine m : game.getMedic()){
			
			laser.draw(m.left(), m.bottom(), m.getLg(), m.getH());
		}
		BattleShip shipBB = game.getShip();
		if (shipBB.getLg() < 20)	{
			ship.draw(shipBB.getX()-10, shipBB.getY()-10, 20, 20);
		} else {
			ship.draw(shipBB.left(), shipBB.bottom(), shipBB.getLg(), shipBB.getH());
		}		
	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		if (gc.getInput().isKeyPressed(Keyboard.KEY_P))	{
			game.getChrono().pause();
			if (gc.isPaused())	{
				gc.setPaused(false);
			} else {
				gc.setPaused(true);
			}
		}
		
		return gc.isPaused() ? this : game.updateGame(gc, this);
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		gameContainer = gc;
		game.start();
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
}
