package org.helmo.cellsoflife.scenes;

import java.awt.Font;
import java.util.List;

import org.helmo.cellsoflife.db.CellsDataBase;
import org.helmo.cellsoflife.domains.entity.RankingEntry;
import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.helmo.cellsoflife.utils.Converter;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

public class RankingScene implements Scene {
	
	private List<RankingEntry> rankScore;
	private List<RankingEntry> rankTime;
	private List<RankingEntry> toShow;
	private boolean showStatusScore;
	private String title;

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image back = new Image("ressources/r.png");
		Image arrowL = new Image("ressources/arrowLeft.png");
		Image arrowR = new Image("ressources/arrowRight.png");
		Font font = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 20);
		TrueTypeFont ttf = new TrueTypeFont(font, true);
		
		back.draw(0, 0, 640, 480);
		arrowL.draw(225, 154, 20, 20);
		arrowR.draw(400, 154, 20, 20);
		painter.setFont(ttf);
		painter.setColor(Color.black);
		painter.drawString(title, 250, 150);
		painter.drawString(GameLanguage.currentLanguage.get("rank"), 20, 180);
		painter.drawString(GameLanguage.currentLanguage.get("player"), 100, 180);
		painter.drawString(GameLanguage.currentLanguage.get("scores"), 275, 180);
		painter.drawString(GameLanguage.currentLanguage.get("times"), 405, 180);
		painter.drawString(GameLanguage.currentLanguage.get("sizes"), 585, 180);
		painter.drawString(GameLanguage.currentLanguage.get("back"), 290, 450);
		
		painter.setColor(Color.darkGray);
		displayArrayRank(painter, 35, 215, toShow);

	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Input charPressed = gc.getInput();
		Scene s = this;
		
		if(gc.getInput().isKeyPressed(Keyboard.KEY_B))	{
			
			s = new WelcomeScene();
			
		} else if (gc.getInput().isKeyPressed(Keyboard.KEY_RIGHT) || gc.getInput().isKeyPressed(Keyboard.KEY_LEFT)){
			
			if (showStatusScore)	{
				toShow = rankTime;
				title = GameLanguage.currentLanguage.get("bestTime");
				showStatusScore = false;
			} else {
				toShow = rankScore;
				title = GameLanguage.currentLanguage.get("bestScore");
				showStatusScore = true;
			}
		} else if (charPressed.isKeyPressed(Keyboard.KEY_ESCAPE))	{
			
			s = new WelcomeScene();
			
		}
		
		charPressed.clearKeyPressedRecord();
		return s;
		
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		
		rankScore = Converter.arrayRankToList(CellsDataBase.selectTopScore(10));
		toShow = rankScore;
		showStatusScore = true;
		title = "Meilleurs score";
		rankTime = Converter.arrayRankToList(CellsDataBase.selectTopTime(10));
		
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Display an array of Score
	 * @param painter Render object
	 * @param x Coordinate X to start display
	 * @param y Coordinate Y to start display
	 * @param rank Array to display (length >= 5)
	 */
	private void displayArrayRank(Graphics painter, int x, int y, List<RankingEntry> ranks)	{
		
		RankingEntry rank;
		String pos = "0";
		
		for (int i = 0; i < ranks.size(); i++)	{
			
			rank = ranks.get(i);
			pos = (i + 1) + "";
			
			painter.drawString(pos, x, y + (i*20));
			painter.drawString(rank.getPlayer(), x+40, y + (i*20));
			painter.drawString(rank.getScore(), x+240, y + (i*20));
			painter.drawString(rank.getTime(), x+370, y + (i*20));
			painter.drawString(rank.getSize(), x+550, y + (i*20));
		}
		
	}

}
