package org.helmo.cellsoflife.scenes;

import java.awt.Font;
import java.util.HashMap;
import java.util.Map;

import org.helmo.cellsoflife.db.CellsDataBase;
import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

public class SaveGameScene implements Scene {
	
	private String playerName = "";
	private int saveScore;
	private String saveTime;
	private boolean winner;
	private boolean[][] backupGrid;
	private Map<Integer, Character> allowedChar = new HashMap<>();

	public SaveGameScene(int score, String time, boolean[][] grid, boolean win) {
		saveScore = score;
		saveTime = time;
		backupGrid = grid;
		winner = win;
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image back = new Image("ressources/w.png");
		Font font = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 20);
		TrueTypeFont ttf = new TrueTypeFont(font, true);
		
		back.draw(0, 0, 640, 480);
		painter.setFont(ttf);
		painter.setColor(Color.black);
		painter.drawString(GameLanguage.currentLanguage.get("name"), 100, 420);
		painter.drawString(playerName, 270, 420);

	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Input charPressed = gc.getInput();
		Scene s = this;
		
		if (playerName.length() < 10)	{
			
			for (int key : allowedChar.keySet())	{
				
				if (charPressed.isKeyPressed(key))	{
					playerName += allowedChar.get(key);
				}
			}
		}
		
		if (charPressed.isKeyPressed(Keyboard.KEY_BACK))	{
			
			if (playerName.length() >0)	{playerName = playerName.substring(0, playerName.length()-1);}
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_RETURN))	{
			
			if (playerName.length() > 0)	{
				CellsDataBase.saveFinishGame(playerName, saveScore, saveTime, winner);
				s =  new GameOverScene(CellsDataBase.checkRankTopScore(playerName, saveScore, saveTime), CellsDataBase.checkRankTopTime(playerName, saveScore, saveTime), backupGrid, saveScore, saveTime, winner);
			}
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_ESCAPE))	{
			
			s = new WelcomeScene();
			
		}
		
		charPressed.clearKeyPressedRecord();
		return s;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		allowedChar.put(Keyboard.KEY_A, 'A');
		allowedChar.put(Keyboard.KEY_B, 'B');
		allowedChar.put(Keyboard.KEY_C, 'C');
		allowedChar.put(Keyboard.KEY_D, 'D');
		allowedChar.put(Keyboard.KEY_E, 'E');
		allowedChar.put(Keyboard.KEY_F, 'F');
		allowedChar.put(Keyboard.KEY_G, 'G');
		allowedChar.put(Keyboard.KEY_H, 'H');
		allowedChar.put(Keyboard.KEY_I, 'I');
		allowedChar.put(Keyboard.KEY_J, 'J');
		allowedChar.put(Keyboard.KEY_K, 'K');
		allowedChar.put(Keyboard.KEY_L, 'L');
		allowedChar.put(Keyboard.KEY_M, 'M');
		allowedChar.put(Keyboard.KEY_N, 'N');
		allowedChar.put(Keyboard.KEY_O, 'O');
		allowedChar.put(Keyboard.KEY_P, 'P');
		allowedChar.put(Keyboard.KEY_Q, 'Q');
		allowedChar.put(Keyboard.KEY_R, 'R');
		allowedChar.put(Keyboard.KEY_S, 'S');
		allowedChar.put(Keyboard.KEY_T, 'T');
		allowedChar.put(Keyboard.KEY_U, 'U');
		allowedChar.put(Keyboard.KEY_V, 'V');
		allowedChar.put(Keyboard.KEY_W, 'W');
		allowedChar.put(Keyboard.KEY_X, 'X');
		allowedChar.put(Keyboard.KEY_Y, 'Y');
		allowedChar.put(Keyboard.KEY_Z, 'Z');
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
}
