package org.helmo.cellsoflife.scenes;

import java.awt.Font;

import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;

public class ParametersScene implements Scene {

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image back = new Image("ressources/p.png");
		Image more = new Image("ressources/max.png");
		Image less = new Image("ressources/min.png");
		Image up = new Image("ressources/arrowUp.png");
		Image down = new Image("ressources/arrowDown.png");
		Image right = new Image("ressources/arrowRight.png");
		Image left = new Image("ressources/arrowLeft.png");
		Font font = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 20);
		Font bigFont = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 40);
		TrueTypeFont ttf = new TrueTypeFont(font, true);
		
		painter.setFont(ttf);
		back.draw(0, 0, 640, 480);
		
		painter.drawString(GameLanguage.currentLanguage.get("chooseSize"), 545, 220);
		painter.drawString(GameLanguage.currentLanguage.get("chooseSpeed"), 50, 220);
		painter.drawString(GameLanguage.currentLanguage.get("chooseLanguage"), 290, 250);
		painter.drawString(GameLanguage.currentLanguage.get("back"), 290, 365);
		
		ttf = new TrueTypeFont(bigFont, true);
		painter.setFont(ttf);
		
		left.draw(175, 290, 45, 45);
		painter.drawString(GameLanguage.langChoose, 230, 285);
		right.draw(450, 290, 45, 45);
		
		up.draw(60, 250, 45, 45);
		painter.drawString(GameParameters.SPEED_INFECTION + "", 50, 285);
		down.draw(60, 330, 45, 45);
		
		more.draw(545, 250, 45, 45);
		painter.drawString(GameParameters.GRID_SIZE + "", 545, 285);
		less.draw(545, 330, 45, 45);
	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Input charPressed = gc.getInput();
		Scene s = this;
		
		if (charPressed.isKeyPressed(Keyboard.KEY_ADD))	{
			
			modifyGridSize(10);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_SUBTRACT))	{
						
			modifyGridSize(-10);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_UP))	{
						
			modifySpeedInfection(10);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_DOWN))	{
						
			modifySpeedInfection(-10);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_RIGHT))	{
			
			switch (GameLanguage.langChoose) {
			case "Francais":
				GameLanguage.setLanguage("English");
				break;
			case "English":
				GameLanguage.setLanguage("Nederlands");
				break;
			case "Nederlands":
				GameLanguage.setLanguage("espanol");
				break;
			case "espanol":
				GameLanguage.setLanguage("Francais");
				break;
			}

		} else if (charPressed.isKeyPressed(Keyboard.KEY_LEFT))	{
			
			switch (GameLanguage.langChoose) {
			case "Francais":
				GameLanguage.setLanguage("espanol");
				break;
			case "espanol":
				GameLanguage.setLanguage("Nederlands");
				break;
			case "Nederlands":
				GameLanguage.setLanguage("English");
				break;
			case "English":
				GameLanguage.setLanguage("Francais");
				break;
			}
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_B) || charPressed.isKeyPressed(Keyboard.KEY_ESCAPE))	{
			
			s = new WelcomeScene();

		} 
		
		return s;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
	
	/**
	 * Change the size of the grid
	 * @param action Value to increase or decrease to actual size
	 */
	private void modifyGridSize(int action)	{
		int temp = GameParameters.GRID_SIZE + action;
		if (temp > 80)	{GameParameters.setGridSize(80);}
		else if (temp < 10)	{GameParameters.setGridSize(10);}
		else {GameParameters.setGridSize(GameParameters.GRID_SIZE + action);}
	}
	
	/**
	 * Change the speed of infection
	 * @param action Value to increase or decrease to actual speed
	 */
	private void modifySpeedInfection(int action)	{
		int temp = GameParameters.SPEED_INFECTION + action;
		if (temp > 100)	{GameParameters.SPEED_INFECTION = 100;}
		else if (temp < 10)	{GameParameters.SPEED_INFECTION = 10;}
		else {GameParameters.SPEED_INFECTION = temp;}
	}

}
