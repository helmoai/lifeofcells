package org.helmo.cellsoflife.scenes;

import java.awt.Font;

import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;
import org.newdawn.slick.TrueTypeFont;

public class GameOverScene implements Scene {
	
	private String congratulationScore = "";
	private String congratulationTime = "";
	private String gameState = "";
	private boolean[][] grid;
	private boolean win;

	public GameOverScene(boolean bestScore, boolean bestTime, boolean[][] grid, int score, String time, boolean win) {
		if (bestScore) {congratulationScore = GameLanguage.currentLanguage.get("topScore") +  "(" + score + ")";}
		if (bestTime) {congratulationTime = GameLanguage.currentLanguage.get("topTime") +  "(" + time.trim() + ")";}
		if (win) {
			gameState = GameLanguage.currentLanguage.get("healed");
		} else {
			gameState = GameLanguage.currentLanguage.get("dead");
		}
		this.grid = grid;
		this.win = win;
	}

	@Override
	public void draw(Graphics painter) throws SlickException {
		
		Image back = new Image("ressources/v.png");
		Font font = new Font("ressources/earthorbiteriral.ttf", Font.ITALIC, 20);
		TrueTypeFont ttf = new TrueTypeFont(font, true);
		painter.setFont(ttf);
		
		painter.setColor(Color.black);
		back.draw(0, 0, 640, 480);
		painter.drawString(gameState, 225, 215);
		painter.drawString(congratulationScore, 20, 360);
		painter.drawString(congratulationTime, 20, 380);
		painter.drawString(GameLanguage.currentLanguage.get("restart"), 20, 435);
		painter.drawString(GameLanguage.currentLanguage.get("ranking"), 20, 455);
		painter.drawString(GameLanguage.currentLanguage.get("menu"), 400, 435);

	}

	@Override
	public Scene update(GameContainer gc, int elapsedTimeSinceLastUpdateInMillis) throws SlickException {
		
		Input charPressed = gc.getInput();
		Scene s = this;
		
		if (win)	{
			this.playSound();
			win = false;		
		}
		
		if (charPressed.isKeyPressed(Keyboard.KEY_R))	{
			
			s = new GameScene(grid);
			s.gainingFocus(gc);
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_M))	{
			
			s = new WelcomeScene();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_ESCAPE))	{
			
			s = new WelcomeScene();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_A))	{
			
			s = new RankingScene();
			s.gainingFocus(gc);
			
		}
		
		gc.getInput().clearKeyPressedRecord();
		return s;
	}

	@Override
	public void gainingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}

	@Override
	public void loosingFocus(GameContainer gc) throws SlickException {
		// TODO Auto-generated method stub
	}
	
	private void playSound()	{
		
		try {
			
			Sound launch = new Sound("ressources/sounds/launch.ogg");
			Sound blast = new Sound("ressources/sounds/blast.ogg");
			Sound twinkle = new Sound("ressources/sounds/twinkle.ogg");
			
			launch.play(1, (float) 0.2);
			
			Thread.sleep(1750);
			blast.play(1, (float) 0.2);
			twinkle.play(1, (float) 0.3);
			
			Thread.sleep(1000);
			twinkle.play(1, (float) 0.3);
			blast.play(1, (float) 0.2);
			
			Thread.sleep(1000);
			blast.play(1, (float) 0.2);
			twinkle.play(1, (float) 0.3);
			
		} catch (InterruptedException e) {
			//e.printStackTrace();
		} catch (SlickException e) {
			//e.printStackTrace();
		}	
	}

}
