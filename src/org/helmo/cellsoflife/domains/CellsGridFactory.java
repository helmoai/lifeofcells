package org.helmo.cellsoflife.domains;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.helmo.cellsoflife.domains.entity.BoundingBox;
import org.helmo.cellsoflife.domains.entity.CellsGrid;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.helmo.cellsoflife.utils.ArrayXDimension;

public class CellsGridFactory extends CellsGrid {

	private static List<BoundingBox> structures = new ArrayList<>();

	/**
	 * Generate the grid for Game of Life
	 * 
	 * @param pct
	 *            Percentage of starting contamination
	 * @return The game grid
	 */
	public static boolean[][] generate(double pct) {

		boolean[][] grid = new boolean[GameParameters.GRID_SIZE][GameParameters.GRID_SIZE];

		Random r = new Random();

		while (ContaminatedCellsCount(grid) < pct) {

			int random = r.nextInt(GameParameters.STRUCTURES.size());

			boolean[][] structure = GameParameters.STRUCTURES.get(random);
			if (structure.length < GameParameters.GRID_SIZE && structure[0].length < GameParameters.GRID_SIZE) {
				try {
					setStructure(grid, structure);
				} catch (StackOverflowError error) {

					int x = r.nextInt(grid[0].length);
					int y = r.nextInt(grid.length);

					if ((x + 3) >= grid[0].length) {
						x -= 6;
					}
					if ((y + 3) >= grid.length) {
						y -= 6;
					}
					ArrayXDimension.copyArray2D(grid, GameParameters.BLINKER, x, y);
				}
			}
		}
		return grid;
	}

	/**
	 * Check a structure grid, at random coordinate
	 * 
	 * @param grid
	 *            Game of Life grid
	 * @param structure
	 *            Structure to copy (GameParameters.STRUCTURES)
	 */
	private static void setStructure(boolean[][] grid, boolean[][] structure) {
		Random r = new Random();
		int x = r.nextInt(grid[0].length);
		int y = r.nextInt(grid.length);

		if ((x + structure[0].length) <= grid[0].length && (y + structure.length) <= grid.length) {
			if (structures.size() == 0) {
				ArrayXDimension.copyArray2D(grid, structure, x, y);
				structures.add(createStructureBox(structure, x, y));
			} else {

				BoundingBox bBs = createStructureBox(structure, x, y);

				for (BoundingBox bB : structures) {
					if (bB.collideWith(bBs)) {
						setStructure(grid, structure);
						return;
					}
				}
				ArrayXDimension.copyArray2D(grid, structure, x, y);
				structures.add(createStructureBox(structure, x, y));
			}
		} else {
			setStructure(grid, structure);
			return;
		}
	}
	
	/**
	 * 
	 * @param structure The structure boolean to convert
	 * @param x coordinate X
	 * @param y coordinate Y
	 * @return BoundingBox for structure
	 */
	private static BoundingBox createStructureBox(boolean[][] structure, int x, int y)	{
		return new BoundingBox(x + structure[0].length / 2, y + structure.length / 2,
				structure[0].length, structure.length);
	}
}
