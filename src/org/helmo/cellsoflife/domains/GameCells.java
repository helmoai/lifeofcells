package org.helmo.cellsoflife.domains;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import org.helmo.cellsoflife.db.CellsDataBase;
import org.helmo.cellsoflife.domains.entity.BattleShip;
import org.helmo.cellsoflife.domains.entity.CellsGrid;
import org.helmo.cellsoflife.domains.entity.Chronometer;
import org.helmo.cellsoflife.domains.entity.Experience;
import org.helmo.cellsoflife.domains.entity.Medicine;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.helmo.cellsoflife.scenes.SaveGameScene;
import org.helmo.cellsoflife.scenes.Scene;
import org.helmo.cellsoflife.scenes.WelcomeScene;
import org.helmo.cellsoflife.utils.Converter;
import org.lwjgl.input.Keyboard;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class GameCells {
	
	private int score;
	private BattleShip ship;
	private Set<Medicine> medic = new HashSet<>();
	private CellsGrid board;
	private CellsGrid backupBoard;
	private Chronometer chrono;
	private long time = System.currentTimeMillis();
	private Experience xp;
	
	public GameCells(int score, String time2, int level, boolean[][] grid, Set<Medicine> medics, boolean[][] backup) {
		this.score = score;
		ship = new BattleShip(GameParameters.LEFT_LIMIT, 455, GameParameters.CELLS_SIZE, GameParameters.CELLS_SIZE);
		if (grid.length > 0)	{
			board = new CellsGrid(grid);
		} else {
			board = new CellsGrid();
		}
		medic = medics;
		chrono = new Chronometer(Converter.stringToTime(time2));
		if (backup.length > 0)	{
			backupBoard = new CellsGrid(backup);
		} else {
			backupBoard = new CellsGrid(board.getGrid());
		}
		xp = new Experience(level, 490, 235);
	}
	
	public GameCells()	{
		this(0, "00:00:00.000000", 0, new boolean[][]{}, new HashSet<Medicine>(), new boolean[][]{});
	}
	
	public GameCells(boolean[][] backup)	{
		this.score = 0;
		ship = new BattleShip(GameParameters.LEFT_LIMIT, 455, GameParameters.CELLS_SIZE, GameParameters.CELLS_SIZE);
		board = new CellsGrid(backup);
		medic = new HashSet<>();
		chrono = new Chronometer();
		backupBoard = new CellsGrid(board.getGrid());
		xp = new Experience(0, 490, 235);
	}
	
	/**
	 * Start game
	 */
	public void start()	{
		chrono.start();
	}
	
	/**
	 * Do the update like the class Scene
	 * @param gc GameContainer
	 * @param s The current scene
	 * @return Scene modified or not
	 * @throws SlickException 
	 */
	public Scene updateGame(GameContainer gc, Scene s) throws SlickException	{
		
		Input charPressed = gc.getInput();
		
		if (System.currentTimeMillis()-time >= GameParameters.SPEED_INFECTION)	{
			board.updateCells();
		}
		
		if (System.currentTimeMillis()-time >= 100)	{
			
			time = System.currentTimeMillis();
			
			for(Medicine m : medic){          
				m.moveUp();
			}
			
			if (charPressed.isKeyDown(Keyboard.KEY_SPACE) || charPressed.isMouseButtonDown(Input.MOUSE_LEFT_BUTTON)){
				
				medic.addAll(ship.shootMedicine(xp.getLevel()));
				
			}
			updateCellIfMedicineContact();
					
		}
		
		if (charPressed.isKeyPressed(Keyboard.KEY_Q) || charPressed.isKeyPressed(Keyboard.KEY_LEFT))	{
						
			ship.moveLeft();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_D) || charPressed.isKeyPressed(Keyboard.KEY_RIGHT))	{
			
			ship.moveRight();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_S))	{
			
			CellsDataBase.registerCurrentGame(score, chrono.timeToString(':', ':', ':', ' '), xp.getXP(), board.getGrid().length, board.gridToString(), Converter.medicToString(medic), GameParameters.SPEED_INFECTION, backupBoard.gridToString());
			s = new WelcomeScene();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_M))	{
			
			s = new WelcomeScene();
			
		} else if (charPressed.isKeyPressed(Keyboard.KEY_ESCAPE))	{
			
			s = new WelcomeScene();
			
		}
		
		if (board.getContamination() <= 1)	{
			
			s = new SaveGameScene(score, chrono.timeToString(':', ':', ':', ' '), backupBoard.getGrid(), true);
			s.gainingFocus(gc);
			chrono.stop();
			
		} else if (board.getContamination() >= 40)	{
			
			s = new SaveGameScene(score, chrono.timeToString(':', ':', ':', ' '), backupBoard.getGrid(), false);
			s.gainingFocus(gc);
			chrono.stop();
			
		}
		
		charPressed.clearKeyPressedRecord();
		return s;
	}
	
	/**
	 * Update cells in grid if a medicine is in contact with it
	 */
	public void updateCellIfMedicineContact()	{
		
		Iterator<Medicine> it = medic.iterator();
		Medicine m;
		int xColumn;
		int yRow;
		
		while (it.hasNext())	{
			m = it.next();
			xColumn = (m.getX()-GameParameters.SPACE_LEFT_WINDOW)/GameParameters.CELLS_SPACE;
			yRow = (m.getY()-GameParameters.SPACE_LEFT_WINDOW)/GameParameters.CELLS_SPACE;
			if(xColumn > -1 && xColumn < GameParameters.GRID_SIZE && yRow > -1 && yRow < GameParameters.GRID_SIZE){
				if(board.getGrid()[xColumn][yRow]){
					board.getGrid()[xColumn][yRow] = false;
					updateScore(100);
					xp.addExperience(1);
				}
			}

			
			if(m.getY() < 0) {
				it.remove();
			}
		}
	}
	
	public void updateScore(int val)	{
		score += val;
	}

	public double getContamination() {
		return (board instanceof CellsGrid) ? board.getContamination() : 0;
	}

	public int getScore() {
		return score;
	}

	public String getTime() {
		return (chrono instanceof Chronometer) ? chrono.timeToString('h', 'm', 's', '\'') : "00h00h00s00'";
	}

	public boolean[][] getGrid() {
		return (board instanceof CellsGrid) ? board.getGrid() : new boolean[][]{};
	}

	public Set<Medicine> getMedic() {
		return medic;
	}

	public BattleShip getShip() {
		return (ship instanceof BattleShip) ? ship : new BattleShip(9, 455, 20, 20);
	}
	
	public Experience getExperience()	{
		return xp;
	}

	public Chronometer getChrono() {
		return chrono;
	}

}
