package org.helmo.cellsoflife.domains.parameters;

import java.util.ArrayList;
import java.util.List;

public class GameParameters {
	
	public static int GRID_SIZE = 30; // Valeur conseillée >=5 et <= 100
	public static int SPACE_LEFT_WINDOW = 7;
	public static int CELLS_SPACE= 420 / GRID_SIZE;
	public static int CELLS_SIZE = CELLS_SPACE - 1;
	public static int LEFT_LIMIT = SPACE_LEFT_WINDOW + (CELLS_SIZE / 2); // Equal to up limit also
	public static int RIGHT_LIMIT = LEFT_LIMIT + (GRID_SIZE-1) * CELLS_SPACE; // Equal to down limit also
	public static int SPEED_INFECTION = 100;
	public static final List<boolean[][]> STRUCTURES = new ArrayList<>();
	public static final boolean[][] BLINKER = new boolean[][]{{false, false, false}, {true , true , true }, {false, false, false}};
	
	/**
	 * Update GRID_SIZE and all value
	 * @param v Value for grid size
	 */
	public static void setGridSize(int v)	{
		GRID_SIZE = v;
		CELLS_SPACE = 420 / GRID_SIZE;
		CELLS_SIZE = CELLS_SPACE - 1;
		LEFT_LIMIT = SPACE_LEFT_WINDOW + (CELLS_SIZE / 2);
		RIGHT_LIMIT = LEFT_LIMIT + (GRID_SIZE-1) * CELLS_SPACE;
	}
	
	/**
	 * Load structure for game in a List
	 */
	public static void loadStructures()	{
		
		// Structure puffeur: https://fr.wikipedia.org/wiki/Puffeur
		
		STRUCTURES.add(new boolean[][]{
			
			{false, true , true , true , true }, // . X X X X
			{true , false, false, false, true }, // X . . . X
			{false, false, false, false, true }, // . . . . X
			{true , false, false, true , false}, // X . . X .
			{false, false, false, false, false}, // . . . . .
			{false, false, false, false, false}, // . . . . .
			{false, true , false, false, false}, // . X . . .
			{false, false, true , false, false}, // . . X . .
			{false, false, true , false, false}, // . . X . .
			{false, true , true , false, false}, // . X X . .
			{true , false, false, false, false}, // X . . . .
			{false, false, false, false, false}, // . . . . .
			{false, false, false, false, false}, // . . . . .
			{false, false, false, false, false}, // . . . . .
			{false, true , true , true , true }, // . X X X X
			{true , false, false, false, true }, // X . . . X
			{false, false, false, false, true }, // . . . . X
			{true , false, false, true , false}, // X . . X .
			{false, false, false, false, false}, // . . . . .
		});
		
		STRUCTURES.add(new boolean[][]{
			
			{true , true , true , false, true }, // X X X . X
			{true , false, false, false, false}, // X . . . .
			{false, false, false, true , true }, // . . . X X
			{false, true , true , false, true }, // . X X . X
			{true , false, true , false, true }, // X . X . X
		});
		
		// Structure stable: https://fr.wikipedia.org/wiki/Structure_stable_(automate_cellulaire)
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false}, // . . . .
			{false, true , true , false}, // . X X .
			{false, true , true , false}, // . X X .
			{false, true , true , false}, // . . . .
		});
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false}, // . . . . . . . . .
			{false, false, false, true , false, false, false, false, false}, // . . . X . . . . .
			{false, false, true , false, true , false, false, false, false}, // . . X . X . . . .
			{false, false, true , false, true , false, false, false, false}, // . . X . X . . . .
			{false, true , true , false, true , true , true , false, false}, // . X X . X X X . .
			{false, false, false, false, false, false, false, true , false}, // . . . . . . . X .
			{false, true , true , false, true , true , true , false, false}, // . X X . X X X . .
			{false, true , true , false, true , false, false, false, false}, // . X X . X . . . .
			{false, false, false, false, false, false, false, false, false}, // . . . . . . . . .
		});
		
		// Structure Canon: https://fr.wikipedia.org/wiki/Canon_(automate_cellulaire)
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true , false, false, false, false, false, false, false, false, false, false, false},
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, true , false, true , false, false, false, false, false, false, false, false, false, false, false},
			{false, false, false, false, false, false, false, false, false, false, false, false, true , true , false, false, false, false, false, false, true , true , false, false, false, false, false, false, false, false, false, false, false, false, true , true },
			{false, false, false, false, false, false, false, false, false, false, false, true , false, false, false, true , false, false, false, false, true , true , false, false, false, false, false, false, false, false, false, false, false, false, true , true },
			{true , true , false, false, false, false, false, false, false, false, true , false, false, false, false, false, true , false, false, false, true , true , false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			{true , true , false, false, false, false, false, false, false, false, true , false, false, false, true , false, true , true , false, false, false, false, true , false, true , false, false, false, false, false, false, false, false, false, false, false},
			{false, false, false, false, false, false, false, false, false, false, true , false, false, false, false, false, true , false, false, false, false, false, false, false, true , false, false, false, false, false, false, false, false, false, false, false},
			{false, false, false, false, false, false, false, false, false, false, false, true , false, false, false, true , false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			{false, false, false, false, false, false, false, false, false, false, false, false, true , true , false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
		});
		// . . . . . . . . . . . . . . . . . . . . . . . . X . . . . . . . . . . .
		// . . . . . . . . . . . . . . . . . . . . . . X . X . . . . . . . . . . .
		// . . . . . . . . . . . . X X . . . . . . X X . . . . . . . . . . . . X X
		// . . . . . . . . . . . X . . . X . . . . X X . . . . . . . . . . . . X X
		// X X . . . . . . . . X . . . . . X . . . X X . . . . . . . . . . . . . .
		// X X . . . . . . . . X . . . X . X X . . . . X . X . . . . . . . . . . .
		// . . . . . . . . . . X . . . . . X . . . . . . . X . . . . . . . . . . .
		// . . . . . . . . . . . X . . . X . . . . . . . . . . . . . . . . . . . .
		// . . . . . . . . . . . . X X . . . . . . . . . . . . . . . . . . . . . .

		
		// Structure Oscillator: https://fr.wikipedia.org/wiki/Oscillateur_(automate_cellulaire)
		
		STRUCTURES.add(BLINKER);
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . .
			{false, false, false, false, false, true , true , true , false, false, false, false, false}, // . . . . . X X X . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . .
			{false, false, false, true , false, true , false, true , false, true , false, false, false}, // . . . X . X . X . X . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . .
			{false, true , false, true , false, false, false, false, false, true , false, true , false}, // . X . X . . . . . X . X .
			{false, true , false, false, false, false, false, false, false, false, false, true , false}, // . X . . . . . . . . . X .
			{false, true , false, true , false, false, false, false, false, true , false, true , false}, // . X . X . . . . . X . X .
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . .
			{false, false, false, true , false, true , false, true , false, true , false, false, false}, // . . . X . X . X . X . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . .
			{false, false, false, false, false, true , true , true , false, false, false, false, false}, // . . . . . X X X . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . 
		});
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . .
			{false, false, false, true , true , true , true , false, false, false}, // . . . X X X X . . .
			{false, false, false, true , false, false, true , false, false, false}, // . . . X . . X . . .
			{false, true , true , true , false, false, true , true , true , false}, // . X X X . . X X X .
			{false, true , false, false, false, false, false, false, true , false}, // . X . . . . . . X .
			{false, true , false, false, false, false, false, false, true , false}, // . X . . . . . . X .
			{false, true , true , true , false, false, true , true , true , false}, // . X X X . . X X X .
			{false, false, false, true , false, false, true , false, false, false}, // . . . X . . X . . .
			{false, false, false, true , true , true , true , false, false, false}, // . . . X X X X . . .
			{false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . .
		});
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
			{false, false, false, true , true , false, true , true , true , true , true , true , false, false, false}, // . . . X X . X X X X X X . . .
			{false, false, false, true , true , false, true , true , true , true , true , true , false, false, false}, // . . . X X . X X X X X X . . .
			{false, false, false, true , true , false, false, false, false, false, false, false, false, false, false}, // . . . X X . . . . . . . . . .
			{false, false, false, true , true , false, false, false, false, false, true , true , false, false, false}, // . . . X X . . . . . X X . . .
			{false, false, false, true , true , false, false, false, false, false, true , true , false, false, false}, // . . . X X . . . . . X X . . .
			{false, false, false, true , true , false, false, false, false, false, true , true , false, false, false}, // . . . X X . . . . . X X . . .
			{false, false, false, false, false, false, false, false, false, false, true , true , false, false, false}, // . . . . . . . . . . X X . . .
			{false, false, false, true , true , true , true , true , true , false, true , true , false, false, false}, // . . . X X X X X X . X X . . .
			{false, false, false, true , true , true , true , true , true , false, true , true , false, false, false}, // . . . X X X X X X . X X . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false}, // . . . . . . . . . . . . . . .
		});
		
		// Structure Ship: https://fr.wikipedia.org/wiki/Vaisseau_(automate_cellulaire)
		
		STRUCTURES.add(new boolean[][]{
			
			{true , false, false, true , false}, // X . . X .
			{false, false, false, false, true }, // . . . . X
			{true , false, false, false, true }, // X . . . X
			{false, true , true , true , true }, // . X X X X
		});
		
		// Structure Jardin d'Eden: https://fr.wikipedia.org/wiki/Jardin_d%27%C3%89den_(automate_cellulaire)
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			{false, true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , false},
			{false, true , true , false, true , false, true , true , true , false, true , true , true , false, true , true , false, true , false, true , false, true , false, true , false, true , false, true , false, true , false, true , false, true , false},
			{false, true , false, true , false, true , true , true , false, true , true , true , false, true , true , true , true , false, true , true , true , false, true , false, true , false, true , false, true , false, true , false, true , false, false},
			{false, true , true , true , true , true , false, true , true , true , false, true , true , true , false, true , true , true , true , false, true , true , true , true , true , true , true , true , true , true , true , true , true , true , false},
			{false, true , false, true , false, true , true , false, true , true , true , false, true , true , true , false, true , false, true , true , true , false, true , false, true , false, true , false, true , false, true , false, true , false, false},
			{false, true , true , true , true , false, true , true , true , false, true , true , true , false, true , true , true , true , true , false, true , true , false, true , false, true , false, true , false, true , false, true , false, true , false},
			{false, false, true , true , false, true , true , true , false, true , true , true , false, true , true , true , false, true , false, true , false, true , true , true , true , true , true , true , true , true , true , true , true , true , false},
			{false, true , true , false, true , true , false, true , true , true , false, true , true , true , false, true , true , false, true , true , true , true , false, true , false, true , false, true , false, true , false, true , false, true , false},
			{false, true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , true , false, true , true , true , true , true , true , true , true , true , true , true , true , true , false},
			{false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false, false},
			
		});
		
		// Structure Mathusalem: https://fr.wikipedia.org/wiki/Mathusalem_(automate_cellulaire)
		
		STRUCTURES.add(new boolean[][]{
			
			{false, false, false, false, true , false, true , false}, // . . . . X . X .
			{true , false, true , false, false, true , false, false}, // X . X . . X . .
			{false, true , false, false, false, true , false, false}, // . X . . . X . .
			{false, true , false, false, false, false, false, true }, // . X . . . . . X
		});
		
		STRUCTURES.add(new boolean[][]{
			
			{false, true , false, false, false, false, false}, // . X . . . . .
			{false, false , false, true, false, false, false}, // . . . X . . .
			{true , true , false, false, true , true , true }, // . X . . X X X
		});
		
		System.out.println("[Life of Cells] Structures loaded");
	}	
}
