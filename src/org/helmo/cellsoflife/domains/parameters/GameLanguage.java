package org.helmo.cellsoflife.domains.parameters;

import java.util.HashMap;
import java.util.Map;

public class GameLanguage {
	
	private static Map<String, Map<String, String>> lang = new HashMap<>();
	public static Map<String, String> currentLanguage = new HashMap<>();
	public static String langChoose;
	
	public static void loadLanguages()	{
		
		Map<String, String> language = new HashMap<>();
		
		language.put("start", "Start game: N");
		language.put("parameters", "Parameters: P");
		language.put("resume", "Resume game: R");
		language.put("quit", "Quit: Q");
		language.put("ranking", "Show rankings: A");
		language.put("chooseSize", "Size");
		language.put("chooseSpeed", "Speed");
		language.put("chooseLanguage", "Language");
		
		language.put("healed", "You are healed !");
		language.put("dead", "You are dead !");
		language.put("topScore", "Congratulation, you are in top 10 of best scores ");
		language.put("topTime", "Congratulation, you are in top 10 of best times ");
		language.put("restart", "Restart: R");
		language.put("menu", "Menu: M");
		
		language.put("infected", "Infected to ");
		language.put("score", "Score: ");
		language.put("time", "Time: ");
		language.put("move", "Move: <- ->");
		language.put("shoot", "Shoot: SPACE");
		language.put("level", "Level: ");
		language.put("save", "Save: S");
		language.put("bonus", "Bonus: ");
		
		language.put("name", "Enter your name: ");
		language.put("back", "Back: B");
		
		language.put("bestScore", "Best scores");
		language.put("bestTime", "Best times");
		language.put("rank", "Ranks");
		language.put("player", "Players");
		language.put("scores", "Scores");
		language.put("times", "Times");
		language.put("sizes", "Sizes");
		
		lang.put("English", language);
		language = new HashMap<>();
		
		language.put("start", "Nouvelle partie: N");
		language.put("parameters", "Parametres: P");
		language.put("resume", "Reprendre partie: R");
		language.put("quit", "Quitter: Q");
		language.put("ranking", "Afficher les classements: A");
		language.put("chooseSize", "Taille");
		language.put("chooseSpeed", "Vitesse");
		language.put("chooseLanguage", "Langue");
		
		language.put("healed", "Vous �tes gu�rit !");
		language.put("dead", "Vous �tes mort !");
		language.put("topScore", "Bravo tu es dans le top 10 des meilleurs scores");
		language.put("topTime", "Bravo tu es dans le top 10 des meilleurs temps");
		language.put("restart", "Recommencer cette partie: R");
		language.put("menu", "Menu: M");
		
		language.put("infected", "Infect� � ");
		language.put("score", "Score: ");
		language.put("time", "Temps: ");
		language.put("move", "Deplacement: <- ->");
		language.put("shoot", "Tir: SPACE");
		language.put("level", "Level: ");
		language.put("save", "Sauvegarder: S");
		language.put("bonus", "Bonus: ");
		
		language.put("name", "Entrez votre nom: ");
		language.put("back", "Retour: B");
		
		language.put("bestScore", "Meilleurs scores");
		language.put("bestTime", "Meilleurs temps");
		language.put("rank", "Rangs");
		language.put("player", "Joueurs");
		language.put("scores", "Scores");
		language.put("times", "Temps");
		language.put("sizes", "Tailles");
		
		lang.put("Francais", language);
		language = new HashMap<>();
		
		language.put("start", "Nieuw partij: N");
		language.put("parameters", "Parameters: P");
		language.put("resume", "Resume een partij: R");
		language.put("quit", "Vertrekken: Q");
		language.put("ranking", "Show ranking: A");
		language.put("chooseSize", "Groot");
		language.put("chooseSpeed", "Snelheid");
		language.put("chooseLanguage", "Taal");

		language.put("healed", "U bent genezen !");
		language.put("dead", "Je dood !");
		language.put("topScore", "Felicitatie je bent in het pip(top) 10 van de beste scores");
		language.put("topTime", "Felicitatie je bent in het pip(top) 10 de leukste momenten");
		language.put("restart", "opnieuw beginnen dit partij: R");
		language.put("menu", "Menu: M");

		language.put("infected", "besmet in ");
		language.put("score", "Score: ");
		language.put("time", "Tijd: ");
		language.put("move", "Verplaatsing: <- - >");
		language.put("shoot", "Trek: SPACE");
		language.put("level", "Niveau:");
		language.put("save", "Beschermen:");
		language.put("bonus", "Bonus:");

		language.put("name", "Vul uw naam in:");
		language.put("back", "Return: B");

		language.put("bestScore", "beter scoort");
		language.put("bestTime", "beste tijden");
		language.put("rank", "rijen");
		language.put("player", "Spelers");
		language.put("scores", "Scores");
		language.put("times", "Tijd");
		language.put("sizes", "Grootte");
		
		lang.put("Nederlands", language);
		language = new HashMap<>();
		
		language.put("start", "Nueva parte: N");
		language.put("parameters", "Par�metros: P");
		language.put("resume", "Repetir una parte: R");
		language.put("quit", "Dejar: Q");
		language.put("ranking", "Ver clasificaciones: a");
		language.put("chooseSize", "Talla");
		language.put("chooseSpeed", "Velocidad");
		language.put("chooseLanguage", "Lengua");

		language.put("healed", "� Eres curado!");
		language.put("dead", "� Moriste!");
		language.put("topScore", "Felicitaci�n eres en el top(se�al) 10 de los mejores tanteos");
		language.put("topTime", "Felicitaci�n eres en el top(se�al) 10 de los mejores tiempos");
		language.put("restart", "Repetir esta parte: R");
		language.put("menu", "Men�: M");

		language.put("infected", "Infectado a�");
		language.put("score", "Tanteo:�");
		language.put("time", "El tiempo:�");
		language.put("move", "Desplazamiento: <- ->");
		language.put("shoot", "Tiro: SPACE");
		language.put("level", "Level:�");
		language.put("save", "Salvaguardar:");
		language.put("bonus", "Bonificaci�n:�");

		language.put("name", "Introduzca su nombre:�");
		language.put("back", "Vuelta: B");

		language.put("bestScore", "Los mejores tanteos");
		language.put("bestTime", "Los mejores tiempos");
		language.put("rank", "Rangos");
		language.put("player", "Jugadores");
		language.put("scores", "Tanteos");
		language.put("times", "El tiempo");
		language.put("sizes", "Tallas");
		
		lang.put("espanol", language);
		language = new HashMap<>();
		
		currentLanguage = lang.get("Francais");
		langChoose = "Francais";
		System.out.println("[Life of Cells] Language loaded");
	}
	
	public static void setLanguage(String str)	{
		currentLanguage = lang.get(str);
		langChoose = str;
	}
}
