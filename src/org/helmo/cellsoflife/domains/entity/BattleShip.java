package org.helmo.cellsoflife.domains.entity;

import java.util.HashSet;
import java.util.Set;

import org.helmo.cellsoflife.domains.parameters.GameParameters;

public class BattleShip extends BoundingBox	{

	public BattleShip(int paramX, int paramY, int paramLg, int paramH) {
		super(paramX, paramY, paramLg, paramH);
	}
	
	/**
	 * Generate a medicine shoot by the ship
	 * @return Multiple Medicine
	 */
	public Set<Medicine> shootMedicine(int level)	{
		int x = super.getX();
		int y = GameParameters.RIGHT_LIMIT + GameParameters.CELLS_SPACE;
		int size = GameParameters.CELLS_SIZE-4;
		int lg = size;
		if (lg <= 0)	{
			lg = 1;
		} else if (lg > 3)	{
			lg = 3;
		}
		
		Set<Medicine> m = new HashSet<>();
		
		if (level >= 10 && level < 30)	{
			m.add(new Medicine(x-GameParameters.CELLS_SPACE, y, lg, size));
			m.add(new Medicine(x+GameParameters.CELLS_SPACE, y, lg, size));
		} else if (level >= 30)	{
			m.add(new Medicine(x-2*GameParameters.CELLS_SPACE, y+GameParameters.CELLS_SPACE, lg, size));
			m.add(new Medicine(x-GameParameters.CELLS_SPACE, y, lg, size));
			m.add(new Medicine(x, y-GameParameters.CELLS_SPACE, lg, size));
			m.add(new Medicine(x+GameParameters.CELLS_SPACE, y, lg, size));
			m.add(new Medicine(x+2*GameParameters.CELLS_SPACE, y+GameParameters.CELLS_SPACE, lg, size));
		} else {
			m.add(new Medicine(x, y, lg, size));
		}
		return m;
	}
	
	/**
	 * Move ship towards the left
	 */
	public void moveLeft() {
		
		setX(getX()-GameParameters.CELLS_SPACE);
		
		if(getX() < GameParameters.LEFT_LIMIT){
			setX(GameParameters.RIGHT_LIMIT);
		}
		
	}
	
	/**
	 * Move ship towards the right
	 */
	public void moveRight() {
		
		setX(getX()+GameParameters.CELLS_SPACE);
		
		if (getX() > GameParameters.RIGHT_LIMIT ){
			
			setX(GameParameters.LEFT_LIMIT);
		}
		
	}

}
