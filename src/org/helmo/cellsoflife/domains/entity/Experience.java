package org.helmo.cellsoflife.domains.entity;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class Experience extends BoundingBox {

	private int level;
	private int experience;
	private String bonus;
	private Sound levelUp;

	public Experience(int xp, int xPos, int yPos) {
		super(xPos, yPos, 170, 30);
		experience = xp;
		updateLevel();
		bonus = "x 1";
	}

	/**
	 * Update level with experiences points
	 */
	private void updateLevel() {

		// Experience = (Level ^ 2) + (6 x Level)
		// 0 = lvl^2 + 6lvl - xp
		int oldLevel = level;
		if (experience < 394) {
			level = calculY(1, 6, -experience);
		} else if ((experience >= 394 && experience < 1628)) {
			level = calculY(2.5, -40.5, 360 - experience);
		} else if (experience >= 1628) {
			level = calculY(4.5, -162.5, 2200 - experience);
		}

		if (level > oldLevel) {
			try {
				levelUp = new Sound("ressources/sounds/levelUp.ogg");
				levelUp.play(1, (float) 0.3);
			} catch (SlickException e) {
				// e.printStackTrace();
			}
		}

		if (level >= 30) {
			bonus = "x 5";
		} else if (level >= 10 && level < 30) {
			bonus = "x 2";
		} else {
			bonus = "x 1";
		}

	}

	/**
	 * Calcul Y in y = ax^2 + bx + c
	 * 
	 * @param a
	 *            First multiplier
	 * @param b
	 *            Second multiplier
	 * @param c
	 *            Third multiplier
	 * @return the value of Y positive
	 */
	private int calculY(double a, double b, double c) {

		int delta = (int) Math.abs(Math.pow(b, 2) - (4 * a * c));
		int y = (int) ((-b + Math.sqrt(delta)) / (2 * a));
		return y;
	}

	/**
	 * Add experiences points
	 * 
	 * @param v
	 *            Number of points to add
	 */
	public void addExperience(int v) {
		experience += v;
		updateLevel();
	}

	public int getLevel() {
		return level;
	}

	public int getXP() {
		return experience;
	}

	/**
	 * Get the size of box to fill for complete Level
	 * 
	 * @return Size of box to fill
	 */
	public double getExperienceAdvancement() {

		int nextLevelExperience = 1;
		int actualLevelExperience = 1;

		if (level <= 16) {
			nextLevelExperience = (int) (Math.pow((level + 1), 2) + (6 * (level + 1)));
			actualLevelExperience = (int) (Math.pow(level, 2) + (6 * level));
		} else if (level >= 17 && level <= 31) {
			nextLevelExperience = (int) (2.5 * Math.pow((level + 1), 2) - (40.5 * (level + 1)) + 360);
			actualLevelExperience = (int) (2.5 * Math.pow(level, 2) - (40.5 * level) + 360);
		} else if (level >= 32) {
			nextLevelExperience = (int) (4.5 * Math.pow((level + 1), 2) - (162.5 * (level + 1)) + 2200);
			actualLevelExperience = (int) (4.5 * Math.pow(level, 2) - (162.5 * level) + 2200);
		}

		double pct = (double) (experience - actualLevelExperience) / (nextLevelExperience - actualLevelExperience);

		return (pct * getLg());
	}

	public String getBonus() {
		return bonus;
	}

}
