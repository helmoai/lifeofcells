package org.helmo.cellsoflife.domains.entity;

import org.helmo.cellsoflife.domains.parameters.GameParameters;

public class Medicine extends BoundingBox	{

	public Medicine(int paramX, int paramY, int paramLg, int paramH) {
		
		super(paramX, paramY, paramLg, paramH);
		
		int temp = 0;
		
		// Debordement
		if (getX() < GameParameters.LEFT_LIMIT)	{
			
			temp = GameParameters.LEFT_LIMIT - getX() - GameParameters.CELLS_SPACE;
			setX(GameParameters.RIGHT_LIMIT-temp);
		} else if (getX() > GameParameters.RIGHT_LIMIT)	{
			
			temp = getX() - GameParameters.RIGHT_LIMIT - GameParameters.CELLS_SPACE;
			setX(GameParameters.LEFT_LIMIT+temp);
		}		
	}
	
	/**
	 * Move medicine to up
	 */
	public void moveUp(){
		this.setY(this.getY() - GameParameters.CELLS_SPACE );
	}
}
