package org.helmo.cellsoflife.domains.entity;

import org.helmo.cellsoflife.domains.CellsGridFactory;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.helmo.cellsoflife.exception.InvalideArrayException;
import org.helmo.cellsoflife.utils.ArrayXDimension;

public class CellsGrid {
	
	private boolean[][] grid;
	
	public CellsGrid()	{
		grid = CellsGridFactory.generate(10);
	}
	
	public CellsGrid(boolean[][] grid)	{
		if (grid == null || grid.length == 0)	{
			throw new InvalideArrayException();
		}
		this.grid = ArrayXDimension.duplicateValueOfArray2D(grid);
	}
	
	/**
	 * Get contamination rate
	 * @return the contamination rate
	 */
	public double getContamination() {
		return ContaminatedCellsCount(grid);
	}
	
	/**
	 * Get percentage of contaminated cell
	 * @param tab Array 2D with contaminated cells
	 * @return the percentage of contaminated cells
	 */
	public static double ContaminatedCellsCount(boolean[][] tab)	{
		
		double nbCells = 0;
		
		for (int i = 0; i < tab.length; i++)	{
			nbCells += ArrayXDimension.nbOccTrue(tab[i]);
		}
		return (nbCells / (GameParameters.GRID_SIZE*GameParameters.GRID_SIZE)) * 100;
	}
	
	/**
	 * Update cells with rules of Game of Life
	 */
	public void updateCells()	{
		
		boolean[][] gridArray = grid;
		boolean[][] temp = ArrayXDimension.duplicateValueOfArray2D(gridArray);
		
		for (int i = 0; i < temp.length; i++)	{
			for (int j = 0; j < temp[i].length; j++)	{
				
				boolean[] block = ArrayXDimension.getAroundPosFromSquareArray(i, j, temp);
				
				int nbC = ArrayXDimension.nbOccTrue(block);
				
				if (temp[i][j])	{
					if (nbC != 3 && nbC != 2)	{
						gridArray[i][j] = false;
					}
				} else {
					if (nbC == 3)	{
						gridArray[i][j] = true;
					}
				}
			}
		}
	}
	
	/**
	 * Convert grid of game to String of 0 and 1
	 * @return The result of convert
	 */
	public String gridToString()	{
		String r = "";
		for (int i = 0; i < grid.length; i++)	{
			for (int j = 0; j < grid[i].length; j++)	{
				if (grid[i][j])	{
					r += "1";
				} else {
					r += "0";
				}
			}
		}
		return r;
	}
	
	public boolean[][] getGrid()	{
		return grid;
	}
	public void setGrid(boolean[][] array)	{
		grid = array;
	}
}
