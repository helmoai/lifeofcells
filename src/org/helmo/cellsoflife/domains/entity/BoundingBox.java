package org.helmo.cellsoflife.domains.entity;

public class BoundingBox {
		
	private int x;
	private int y;
	private int lg;
	private int h;
	
	public BoundingBox(int paramX, int paramY, int paramLg, int paramH)	{
		x = paramX;
		y = paramY;
		lg = paramLg;
		h = paramH;
	}
	
	/**
	 * Check if a box collide with a other box
	 * @param b A BoundingBox
	 * @return true if collide, else false
	 */
	public boolean collideWith(BoundingBox b)	{
		
		double distHor1 = Math.abs(this.right()-b.left());
		double distHor2 = Math.abs(b.right()-this.left());
		
		double maxHor = Math.max(distHor1, distHor2);
		double distVer1 = Math.abs(this.top()-b.bottom());
		double distVer2 = Math.abs(b.top()-this.bottom());
		double maxVer = Math.max(distVer1, distVer2);
		
		return (maxHor < (b.lg + lg) && maxVer < (b.h + h));
	}
	
	/**
	 * Get x coordinate of right side
	 * @return coordinate x
	 */
	public int right()	{
		return x + (lg >> 2);
	}
	/**
	 * Get x coordinate of left side
	 * @return coordinate x
	 */
	public int left()	{
		return x - (lg >> 2);
	}
	/**
	 * Get y coordinate of top side
	 * @return coordinate y
	 */
	public int top()	{
		return y + (h >> 2);
	}
	/**
	 * Get y coordinate of bottom side
	 * @return coordinate y
	 */
	public int bottom()	{
		return y - (h >> 2);
	}
	
	public void setX(int val)	{
		x = val;
	}
	public void setY(int val)	{
		y = val;
	}

	public int getX()	{
		return x;
	}
	public int getY()	{
		return y;
	}
	public int getLg()	{
		return lg;
	}
	public int getH()	{
		return h;
	}
}

