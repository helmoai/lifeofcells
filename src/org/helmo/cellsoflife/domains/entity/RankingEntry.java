package org.helmo.cellsoflife.domains.entity;

public class RankingEntry {
	
	private String score;
	private String time;
	private String player;
	private String size;
	
	public RankingEntry(String pScore, String pTime, String pPlayer, String pSize)	{
		score = pScore;
		time = pTime;
		player = pPlayer;
		size = pSize;
	}
	
	public String getScore() {
		return score;
	}

	public String getTime() {
		return time;
	}

	public String getPlayer() {
		return player;
	}

	public String getSize() {
		return size;
	}
}
