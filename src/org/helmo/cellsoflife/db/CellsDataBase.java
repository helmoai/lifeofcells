package org.helmo.cellsoflife.db;

import org.helmo.cellsoflife.domains.parameters.GameParameters;

import helmo.nhpack.NHDatabaseSession;
import helmo.nhpack.db.ConnectionConfig;
import helmo.nhpack.db.SqlServerConnectionConfig;
import helmo.nhpack.db.SqlStatement;
import helmo.nhpack.exceptions.NHPackException;

public class CellsDataBase implements DbConfigParameters	{
	
	private static final ConnectionConfig config = new SqlServerConnectionConfig();
	static {
		config.withHost(DB_HOST);
		config.withUserName(DB_USER);
		config.withDatabase(DB_NAME);
		config.withPassword(DB_PASSWORD);
	}
	
	/**
	 * Select top score in database
	 * @param top Number of row asked
	 * @return The results of database request
	 */
	public static String[][] selectTopScore(int top) {
		// SELECT TOP (10) * FROM mscol_user ORDER BY user_score DESC
		String[][] results = {{"No player", "No score", "No time"}};
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			SqlStatement request = session.createStatement("SELECT TOP (" + top + ") * FROM mscol_user ORDER BY user_score DESC");
			
			results = request.executeQuery();
			System.out.println(session.getLastError());
			
		} catch(NHPackException e) {
			
			e.printStackTrace();
		}
		return results;
	}
	
	/**
	 * Select top time in database
	 * @param top Number of row asked
	 * @return The results of database request
	 */
	public static String[][] selectTopTime(int top)	{
		// SELECT TOP (10) * FROM mscol_user ORDER BY user_time ASC
		String[][] results = {{"No player", "No score", "No time"}};
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			SqlStatement request = session.createStatement("SELECT TOP (" + top + ") * FROM mscol_user WHERE user_win = 1 ORDER BY user_time ASC");
			
			results = request.executeQuery();
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	/**
	 * Save a game in database
	 * @param score Score to save
	 * @param time Time to save
	 * @param contamination Rate of contamination to save
	 * @param size Grid size
	 * @param gridString Grid of game
	 */
	public static void registerCurrentGame(int score, String time, int level, int size, String gridString, String medicString, int speed, String backup) {
		// INSERT INTO mscol_game (g_score, g_time, g_contamination, g_size) VALUES
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			if (session.openTransaction())	{
				
				SqlStatement request = session.createStatement("INSERT INTO mscol_game (g_score, g_time, g_level, g_size, g_grid, g_medicine, g_speed, g_backup) VALUES (" + score + ",'" + time + "', " + level + ", " + size + ", '" + gridString +  "', '" + medicString + "', " + speed + ", '" + backup + "')");
				
				if(request.executeUpdate() <= 0) {
					throw new RuntimeException(session.getLastError());
				}
				
				if (!session.commit())	{
					throw new RuntimeException(session.getLastError());
				}
			} else {
				throw new RuntimeException(session.getLastError());
			}
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Save finish game in database
	 * @param name Player name to save
	 * @param score Score to save
	 * @param time Time to save
	 */
	public static void saveFinishGame(String name, int score, String time, boolean win) {
		// INSERT INTO mscol_user (user_name, user_score, user_time) VALUES
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			byte winner = 0;
			if (win) {winner = 1;}
			SqlStatement request = session.createStatement("INSERT INTO mscol_user (user_name, user_score, user_time, user_size, user_win) VALUES ('" + name + "'," + score + ", '" + time + "', " + GameParameters.GRID_SIZE + ", " + winner + ")");
			
			if(request.executeUpdate() <= 0) {
				throw new RuntimeException(session.getLastError());
			}
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Get a save game to resume it
	 * @return Array with game info
	 */
	public static String[][] getSaveGame() {
		// SELECT TOP (1) * FROM mscol_game
		String[][] results = {{"No score", "No time", "No contamination"}};
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			SqlStatement request = session.createStatement("SELECT TOP (1) * FROM mscol_game");
			
			results = request.executeQuery();
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	/**
	 * Check if a player is in top 10 of time
	 * @param name Player name to check
	 * @param score Player score to check
	 * @param time Player time to check
	 * @return true if player is in top 10, else false
	 */
	public static boolean checkRankTopTime(String name, int score, String time) {
		
		String[][] results = {};
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			SqlStatement request = session.createStatement("SELECT * FROM Time_top_10 "
					+ "INTERSECT "
					+ "SELECT * FROM mscol_user WHERE user_name = '" + name + "' AND user_score = " + score + " AND user_time = '" + time + "' AND user_win = 1" 
					+ "ORDER BY user_time ASC");
			
			results = request.executeQuery();
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
		return (results.length > 0) ? true : false;
	}
	
	/**
	 * Check if a player is in top 10 of score
	 * @param name Player name to check
	 * @param score Player score to check
	 * @param time Player time to check
	 * @return true if player is in top 10, else false
	 */
	public static boolean checkRankTopScore(String name, int score, String time) {
		
		String[][] results = {};
		try(NHDatabaseSession session = new NHDatabaseSession(config)){
			
			SqlStatement request = session.createStatement("SELECT * FROM Score_top_10 "
					+ "INTERSECT "
					+ "SELECT * FROM mscol_user WHERE user_name = '" + name + "' AND user_score = " + score + " AND user_time = '" + time + "'"
					+ "ORDER BY user_score DESC");
			
			results = request.executeQuery();
			
		} catch(NHPackException e) {
			e.printStackTrace();
		}
		return (results.length > 0) ? true : false;
	}
}
