package org.helmo.cellsoflife.exception;

public class InvalideArrayException extends RuntimeException	{
	
	/**
	 * Hash key to identify this class
	 */
	private static final long serialVersionUID = 6811410197109L;

	public InvalideArrayException()	{
		super("Array is null or his length = 0");
	}
}
