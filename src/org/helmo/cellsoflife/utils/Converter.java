package org.helmo.cellsoflife.utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

import org.helmo.cellsoflife.domains.entity.Medicine;
import org.helmo.cellsoflife.domains.entity.RankingEntry;

public class Converter {
	
	/**
	 * Convert String in time in millis
	 * @param str String (format HH:MM:SS.mmmmmmm)
	 * @return time in milliSeconds or 0 if pattern not ok
	 */
	public static long stringToTime(String str)	{
		
		if (!Pattern.matches("\\d{2}:\\d{2}:\\d{2}\\.\\d{7}", str))	{
			return 0;
		}
		
		long time = 0;
		
		int hours = Integer.parseInt(str.substring(0,2));
		int minutes = Integer.parseInt(str.substring(3,5));
		int secondes = Integer.parseInt(str.substring(6,8));
		int centiemes = Integer.parseInt(str.substring(9, 11));
		
		time += (hours * 360000);
		time += (minutes * 6000);
		time += (secondes * 100);
		time += centiemes;
		
		time *= 10;
		
		return time;
		
	}
	
	/**
	 * Convert String of 0 and 1 to grid for game
	 * @return The result of convert
	 */
	public static boolean[][] stringtoGrid(String grid2) {
		
		int size = (int) Math.sqrt(grid2.length());
		
		boolean[][] result = new boolean[size][size];
		int row = 0;
		int col = 0;
		
		for (int i = 0; i < grid2.length(); i++)	{
			
			if (col >= size)	{
				row++;
				col = 0;
			}
			//System.out.println(row + " " + col);
			if (grid2.substring(i, i+1).equals("1"))	{
				result[row][col] = true;
			} else {
				result[row][col] = false;
			}
			col++;
		}
		return result;
	}
	
	/**
	 * Convert collection of Medicine to String
	 * @param medic Collectio of Medicine
	 * @return String off all Medecine info
	 */
	public static String medicToString(Set<Medicine> medic) {
		String r = "";
		
		Medicine m;
		Iterator<Medicine> it = medic.iterator();
		while (it.hasNext())	{
			m = it.next();
			r += m.getX();
			r += ":";
			r += m.getY();
			r += ":";
			r += m.getLg();
			r += ":";
			r += m.getH();
			r += "-";
		}
		return r;
	}
	
	/**
	 * Convert String of database to Medicine usefull
	 * @param medic String of all coordinate and size
	 * @return Collection of Medicine
	 */
	public static Set<Medicine> stringToMedic(String medic) {
		
		Set<Medicine> m = new HashSet<>();
		if (medic.length() > 0)	{
			String[] coordinates = medic.split("-");
			for (int i = 0; i < coordinates.length; i++)	{
				String[] coordXY = coordinates[i].split(":");
				m.add(new Medicine(Integer.parseInt(coordXY[0]), Integer.parseInt(coordXY[1]), Integer.parseInt(coordXY[2]), Integer.parseInt(coordXY[3])));
			}
		}
		
		
		return m;
	}
	
	/**
	 * Convert array to List of rankingEntry
	 * @param array Array of user information
	 * @return Collection of RankingEntry
	 */
	public static List<RankingEntry> arrayRankToList(String[][] array) {
		
		List<RankingEntry> ranks = new ArrayList<>();
		
		for (int i = 0; i < array.length; i++)	{
			
			String player = array[i][1];
			String score = array[i][2];
			String time = array[i][3];
			time = time.replaceFirst(":", "h");
			time = time.replaceFirst(":", "m");
			time = time.replaceFirst("\\.", "s");
			time = time.substring(0, time.indexOf("s")+3);
			time += "'";
			String size = array[i][4];
			
			ranks.add(new RankingEntry(score, time, player, size));
			
		}
		return ranks;
	}

}
