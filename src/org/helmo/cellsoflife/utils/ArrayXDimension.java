package org.helmo.cellsoflife.utils;

public class ArrayXDimension {
	
	/**
	 * Copy value (boolean) of array in an other array
	 * @param src Array to copy
	 * @return Array copy of src
	 */
	public static boolean[][] duplicateValueOfArray2D(boolean[][] src)	{
		boolean[][] dest =  new boolean[src.length][src[0].length];
		
		for (int i = 0; i < src.length; i++)	{
			for (int j = 0; j < src[i].length; j++)	{
				dest[i][j] = src[i][j];
			}
		}
		return dest;
	}
	
	/**
	 * Get value in column of array
	 * @param array Array
	 * @param column Desired column
	 */
	public static boolean[] getColoneValueInArray2D(boolean[][] array, int column){
		
		boolean[] dest = new boolean[array.length];
		for(int i = 0; i < array.length; i++){
			dest[i] = array[i][column];
		}
		return dest;
		
	}
	
	/**
	 * Get value around pos in an array (boolean)
	 * @param i Pos row
	 * @param j Pos column
	 * @param array Array  treated
	 * @return Array with value around pos(i, j)
	 */
	public static boolean[] getAroundPosFromSquareArray(int i, int j, boolean[][] array)	{
		
		boolean[] block = {};
		
		if (array != null && array.length != 0)	{
			int rowUp = i+1;
			int rowDown = i-1;
			int colLeft = j-1;
			int colRight = j+1;
			
			if (rowUp == array.length)	{
				rowUp = 0;
			}
			if (rowDown == -1)	{
				rowDown = array.length-1;
			}
			if (colLeft == -1)	{
				colLeft = array.length-1;
			}
			if (colRight == array.length)	{
				colRight = 0;
			}
			boolean c1 = array[rowUp][colLeft]; boolean c2 = array[rowUp][j]; boolean c3 = array[rowUp][colRight];
			boolean c4 = array[i][colLeft]; 									boolean c5 = array[i][colRight];
			boolean c6 = array[rowDown][colLeft]; boolean c7 = array[rowDown][j]; boolean c8 = array[rowDown][colRight];
			
			block = new boolean[]{c1,c2,c3,c4,c5,c6,c7,c8};
		}
		return block;
	}
	
	/**
	 * Get value around pos in an array (Integer)
	 * @param i Pos row
	 * @param j Pos column
	 * @param array Array  treated
	 * @return Array with value around pos(i, j)
	 */
	public static int[] getAroundPosFromSquareArray(int i, int j, int[][] array)	{
		// Methode pour demontrer l'algorithme en test
		int[] block = {};
		
		if (array != null && array.length != 0)	{
			int rowUp = i+1;
			int rowDown = i-1;
			int colLeft = j-1;
			int colRight = j+1;
			
			if (rowUp == array.length)	{
				rowUp = 0;
			}
			if (rowDown == -1)	{
				rowDown = array.length-1;
			}
			if (colLeft == -1)	{
				colLeft = array.length-1;
			}
			if (colRight == array.length)	{
				colRight = 0;
			}
			int c1 = array[rowUp][colLeft]; int c2 = array[rowUp][j]; int c3 = array[rowUp][colRight];
			int c4 = array[i][colLeft]; 									int c5 = array[i][colRight];
			int c6 = array[rowDown][colLeft]; int c7 = array[rowDown][j]; int c8 = array[rowDown][colRight];
			
			block = new int[]{c1,c2,c3,c4,c5,c6,c7,c8};
		}
		return block;
	}
	
	/**
	 * Copy structure in Game of Life grid
	 * @param grid grid Game of Life grid
	 * @param structure Structure to copy
	 * @param x Coordinate X to start copy
	 * @param y Coordinate Y to start copy
	 */
	public static void copyArray2D(boolean[][] grid, boolean[][] structure, int x, int y)	{
		for(int i = 0; i < structure.length; i++){
			for(int j = 0; j < structure[0].length; j++)	{
				grid[x+j][y+i] = structure[i][j];
			}
		}
	}
	
	/**
	 * Count number of contaminated cells around a value in array
	 * @param array
	 * @return
	 */
	public static int nbOccTrue(boolean[] array)	{
		int nb = 0;
		for (int i = 0; i < array.length; i++)	{
			if (array[i])	{
				nb++;
			}
		}
		return nb;
	}
}
