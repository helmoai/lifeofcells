package org.helmo.cellsoflife.core;

import org.helmo.cellsoflife.domains.parameters.GameLanguage;
import org.helmo.cellsoflife.domains.parameters.GameParameters;
import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.SlickException;

public class Program {
	
	public static void main(String[] args)	{
			
		try {
			AppGameContainer appgc;
			appgc = new AppGameContainer(new CellsGame());
			appgc.setDisplayMode(640, 480, false);
			GameParameters.loadStructures();
			GameLanguage.loadLanguages();
			appgc.start();
		} catch (SlickException ex){
			System.err.println("Erreur interne � Slick2D");
			ex.printStackTrace(System.err);
		}
		
	}
}
