package org.helmo.cellsoflife.core;

import org.helmo.cellsoflife.domains.entity.BattleShip;
import org.helmo.cellsoflife.domains.entity.CellsGrid;
import org.helmo.cellsoflife.scenes.Scene;
import org.helmo.cellsoflife.scenes.WelcomeScene;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class CellsGame extends BasicGame	{
	
	private Scene currentScene;

	public CellsGame() {
		super("Helmo - Activit� int�grative - LifeOfCells");
	}
	public CellsGame(CellsGrid pGrid, BattleShip pShip)	{
		super("Helmo - Activit� int�grative - LifeOfCells");
	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		gc.setShowFPS(false);
		currentScene = new WelcomeScene();
	}
	
	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		
		currentScene.draw(g);
	}

	@Override
	public void update(GameContainer gc, int t) throws SlickException {
		
		currentScene = currentScene.update(gc, t);
	}
}
